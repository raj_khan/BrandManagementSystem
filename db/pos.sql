-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 11, 2017 at 05:29 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pos`
--

-- --------------------------------------------------------

--
-- Table structure for table `batch_info`
--

CREATE TABLE `batch_info` (
  `BATCH_ID` int(10) NOT NULL COMMENT 'Primary Key',
  `RECEIVE_ID` int(10) NOT NULL COMMENT 'Foregion Key receive_mst',
  `PROD_CODE` varchar(20) NOT NULL COMMENT 'product unique Code',
  `MFG_DATE` date NOT NULL COMMENT 'Product mfg date',
  `EXP_DATE` date NOT NULL COMMENT 'Product exp date',
  `IS_ACTIVE` tinyint(1) DEFAULT '1' COMMENT 'It keeps boolean value representing Active/Inactive. e.g. 1 = Active, 0 = Inactive',
  `CRE_BY` int(10) UNSIGNED DEFAULT NULL,
  `CRE_DT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UPD_BY` int(10) UNSIGNED DEFAULT NULL,
  `UPD_DT` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `customer_info`
--

CREATE TABLE `customer_info` (
  `CUST_ID` bigint(20) NOT NULL COMMENT 'Primary Key',
  `CUST_NAME` varchar(250) NOT NULL COMMENT 'Full Name of Customer',
  `MOBILE_NO` bigint(20) NOT NULL COMMENT 'Customer Mobile Number',
  `CUST_TYPE` varchar(20) DEFAULT NULL COMMENT 'CUSTOMER TYPE',
  `IS_ACTIVE` tinyint(1) DEFAULT '1' COMMENT 'It keeps boolean value representing Active/Inactive. e.g. 1 = Active, 0 = Inactive',
  `CRE_BY` int(10) UNSIGNED DEFAULT NULL,
  `CRE_DT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UPD_BY` int(10) UNSIGNED DEFAULT NULL,
  `UPD_DT` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `invoice_chd`
--

CREATE TABLE `invoice_chd` (
  `INVOICE_CHD_ID` int(10) NOT NULL COMMENT 'Primary Key',
  `INVOICE_ID` int(10) NOT NULL COMMENT 'Primary Key of invoice_mst',
  `ORDER_NO` varchar(250) NOT NULL COMMENT 'Customer Order Number From invoice_mst',
  `CUST_CODE` varchar(250) NOT NULL COMMENT 'Customer Code From sa_customer',
  `PROD_CODE` varchar(250) NOT NULL COMMENT 'Product Cood From sa_product',
  `QUANTITY` int(10) NOT NULL COMMENT 'Total Product Quantity you sell',
  `SELL_PRICE` int(10) NOT NULL COMMENT 'Product selling price',
  `TOTAL_AMT` int(10) NOT NULL COMMENT 'Product total Selling price == QUANTITY * SELL_PRICE',
  `IS_ACTIVE` tinyint(1) DEFAULT '1' COMMENT 'It keeps boolean value representing Active/Inactive. e.g. 1 = Active, 0 = Inactive',
  `CRE_BY` int(10) UNSIGNED DEFAULT NULL,
  `CRE_DT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UPD_BY` int(10) UNSIGNED DEFAULT NULL,
  `UPD_DT` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `invoice_mst`
--

CREATE TABLE `invoice_mst` (
  `INVOICE_ID` int(20) NOT NULL COMMENT 'Primary Key',
  `ORDER_NO` varchar(250) NOT NULL COMMENT 'Customer Order Number',
  `ORDER_DATE` date NOT NULL COMMENT 'Order Date',
  `CUST_CODE` varchar(250) NOT NULL COMMENT 'Customer Code From sa_customer',
  `TOTAL_AMT` int(10) NOT NULL COMMENT 'Total Amount of Order',
  `IS_ACTIVE` tinyint(1) DEFAULT '1' COMMENT 'It keeps boolean value representing Active/Inactive. e.g. 1 = Active, 0 = Inactive',
  `CRE_BY` int(10) UNSIGNED DEFAULT NULL,
  `CRE_DT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UPD_BY` int(10) UNSIGNED DEFAULT NULL,
  `UPD_DT` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `money_collection`
--

CREATE TABLE `money_collection` (
  `COLL_NO` int(10) NOT NULL COMMENT 'Primary Key',
  `COLL_DATE` date NOT NULL COMMENT 'Money Collection Date',
  `CUST_ID` int(20) NOT NULL COMMENT 'Foregion  Key of Customer_info table',
  `INVOICE_NO` int(10) NOT NULL COMMENT 'Foregion Key invoice_mst',
  `MONEY_RECEIPT_NO` int(10) NOT NULL COMMENT 'Foregion Key receive_mst',
  `COLL_AMT` date NOT NULL COMMENT 'Product mfg date',
  `PAY_MODE` date NOT NULL COMMENT 'Product exp date',
  `IS_ACTIVE` tinyint(1) DEFAULT '1' COMMENT 'It keeps boolean value representing Active/Inactive. e.g. 1 = Active, 0 = Inactive',
  `CRE_BY` int(10) UNSIGNED DEFAULT NULL,
  `CRE_DT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UPD_BY` int(10) UNSIGNED DEFAULT NULL,
  `UPD_DT` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `order_chd`
--

CREATE TABLE `order_chd` (
  `ORDER_CHD_ID` int(10) NOT NULL COMMENT 'Primary Key',
  `ORDER_ID` int(10) NOT NULL COMMENT 'foregion Key of order_mst',
  `PROD_CODE` varchar(20) NOT NULL COMMENT 'product unique Code',
  `QUANTITY` int(10) NOT NULL COMMENT 'Total Product Quantity you Receive',
  `UNIT_PRICE` int(10) NOT NULL COMMENT 'Product unit price',
  `TOTAL_AMT` int(10) NOT NULL COMMENT 'Product total Selling price == QUANTITY * SELL_PRICE',
  `IS_ACTIVE` tinyint(1) DEFAULT '1' COMMENT 'It keeps boolean value representing Active/Inactive. e.g. 1 = Active, 0 = Inactive',
  `CRE_BY` int(10) UNSIGNED DEFAULT NULL,
  `CRE_DT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UPD_BY` int(10) UNSIGNED DEFAULT NULL,
  `UPD_DT` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `order_mst`
--

CREATE TABLE `order_mst` (
  `ORDER_ID` int(10) NOT NULL COMMENT 'Primary Key',
  `PO_NO` varchar(250) NOT NULL COMMENT 'User Define Product Order no',
  `PO_DATE` date NOT NULL COMMENT 'Product order Date',
  `SUPP_CODE` varchar(100) NOT NULL COMMENT 'Supplier Code',
  `TOTAL_AMT` int(10) NOT NULL COMMENT 'Product total Selling price == QUANTITY * SELL_PRICE',
  `IS_ACTIVE` tinyint(1) DEFAULT '1' COMMENT 'It keeps boolean value representing Active/Inactive. e.g. 1 = Active, 0 = Inactive',
  `CRE_BY` int(10) UNSIGNED DEFAULT NULL,
  `CRE_DT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UPD_BY` int(10) UNSIGNED DEFAULT NULL,
  `UPD_DT` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `receive_chd`
--

CREATE TABLE `receive_chd` (
  `RECEIVE_CHD_ID` int(10) NOT NULL COMMENT 'Primary Key',
  `RECEIVE_ID` int(10) NOT NULL COMMENT 'Forigion Key from receive_mst',
  `GRN_NO` varchar(250) NOT NULL COMMENT 'Good Receive Number From receive_mst',
  `PROD_CODE` varchar(250) NOT NULL COMMENT 'Product Cood From sa_product',
  `QUANTITY` int(10) NOT NULL COMMENT 'Total Product Quantity you Receive',
  `UNIT_PRICE` int(10) NOT NULL COMMENT 'Receive Product unit price',
  `TOTAL_AMT` int(10) NOT NULL COMMENT 'Receive Product total price == QUANTITY * UNIT_PRICE',
  `BATCH_NO` varchar(30) DEFAULT NULL COMMENT 'This Product Batch No',
  `IS_ACTIVE` tinyint(1) DEFAULT '1' COMMENT 'It keeps boolean value representing Active/Inactive. e.g. 1 = Active, 0 = Inactive',
  `CRE_BY` int(10) UNSIGNED DEFAULT NULL,
  `CRE_DT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UPD_BY` int(10) UNSIGNED DEFAULT NULL,
  `UPD_DT` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `receive_mst`
--

CREATE TABLE `receive_mst` (
  `RECEIVE_ID` int(10) NOT NULL COMMENT 'Primary Key',
  `PO_NO` varchar(250) DEFAULT NULL COMMENT 'User Define Product Order no ',
  `GRN_NO` varchar(250) NOT NULL COMMENT 'Good Receive Number',
  `GRN_DATE` date NOT NULL COMMENT 'Good Receive Date',
  `SUPP_CODE` varchar(250) NOT NULL COMMENT 'Good Receive Supplier Code From suplier_info',
  `TOTAL_AMT` bigint(20) NOT NULL COMMENT 'Total Good Receive Amount',
  `IS_ACTIVE` tinyint(1) DEFAULT '1' COMMENT 'It keeps boolean value representing Active/Inactive. e.g. 1 = Active, 0 = Inactive',
  `CRE_BY` int(10) UNSIGNED DEFAULT NULL,
  `CRE_DT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UPD_BY` int(10) UNSIGNED DEFAULT NULL,
  `UPD_DT` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sa_category`
--

CREATE TABLE `sa_category` (
  `CAT_ID` int(20) NOT NULL COMMENT 'Primary Key',
  `CAT_NAME` varchar(250) NOT NULL COMMENT 'Full Name of Product Category',
  `IS_ACTIVE` tinyint(1) DEFAULT '1' COMMENT 'It keeps boolean value representing Active/Inactive. e.g. 1 = Active, 0 = Inactive',
  `CRE_BY` int(10) UNSIGNED DEFAULT NULL,
  `CRE_DT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UPD_BY` int(10) UNSIGNED DEFAULT NULL,
  `UPD_DT` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sa_employee`
--

CREATE TABLE `sa_employee` (
  `EMP_ID` int(10) NOT NULL COMMENT 'Primary Key',
  `FULL_NAME` varchar(100) NOT NULL COMMENT 'Full Name',
  `GENDER` varchar(10) NOT NULL COMMENT 'Gender of User',
  `EMAIL` varchar(100) DEFAULT NULL COMMENT 'Email ID for Login',
  `MOBILE_NO` varchar(15) NOT NULL COMMENT 'User mobile no for Login or notification purpose',
  `ADDRESS` varchar(300) DEFAULT NULL COMMENT 'User full Address',
  `IMAGE` varchar(100) DEFAULT NULL COMMENT 'User image',
  `IS_ACTIVE` tinyint(1) DEFAULT '1' COMMENT 'It keeps boolean value representing Active/Inactive. e.g. 1 = Active, 0 = Inactive',
  `CRE_BY` int(10) UNSIGNED DEFAULT NULL,
  `CRE_DT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UPD_BY` int(10) UNSIGNED DEFAULT NULL,
  `UPD_DT` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sa_product`
--

CREATE TABLE `sa_product` (
  `PROD_ID` int(10) NOT NULL COMMENT 'Primary Key',
  `PROD_CODE` varchar(20) NOT NULL COMMENT 'product unique Code',
  `PROD_NAME` varchar(200) NOT NULL COMMENT 'Full Name of Product',
  `CAT_ID` int(10) DEFAULT NULL COMMENT 'Primary Key of product category table',
  `SUBCAT_ID` int(10) DEFAULT NULL COMMENT 'Primary Key of product Sub category table',
  `IMAGE` varchar(100) DEFAULT NULL COMMENT 'Product Image',
  `P_PRICE` int(11) NOT NULL COMMENT 'perses Price',
  `S_PRICE` int(11) NOT NULL COMMENT 'Selling Price',
  `IS_ACTIVE` tinyint(1) DEFAULT '1' COMMENT 'It keeps boolean value representing Active/Inactive. e.g. 1 = Active, 0 = Inactive',
  `CRE_BY` int(10) UNSIGNED DEFAULT NULL,
  `CRE_DT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UPD_BY` int(10) UNSIGNED DEFAULT NULL,
  `UPD_DT` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sa_sub_category`
--

CREATE TABLE `sa_sub_category` (
  `SUBCAT_ID` int(10) NOT NULL COMMENT 'Primary Key',
  `CAT_ID` int(10) NOT NULL COMMENT 'Primary Key of sa_category',
  `SUBCAT_NAME` varchar(250) NOT NULL COMMENT 'Full Name of Product Sub Category',
  `IS_ACTIVE` tinyint(1) DEFAULT '1' COMMENT 'It keeps boolean value representing Active/Inactive. e.g. 1 = Active, 0 = Inactive',
  `CRE_BY` int(10) UNSIGNED DEFAULT NULL,
  `CRE_DT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UPD_BY` int(10) UNSIGNED DEFAULT NULL,
  `UPD_DT` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sa_users`
--

CREATE TABLE `sa_users` (
  `USER_ID` int(10) NOT NULL COMMENT 'Primary Key',
  `EMP_ID` int(10) NOT NULL COMMENT 'Foregin Key of sa_employee table',
  `USER_TYPE` varchar(10) NOT NULL COMMENT 'E.G. N = Internal User, C = Citizen',
  `USERNAME` varchar(60) NOT NULL COMMENT 'Username for Login',
  `PASSWORD` varchar(100) NOT NULL COMMENT 'Password for Login',
  `IS_ACTIVE` tinyint(1) DEFAULT '1' COMMENT 'It keeps boolean value representing Active/Inactive. e.g. 1 = Active, 0 = Inactive',
  `CRE_BY` int(10) UNSIGNED DEFAULT NULL,
  `CRE_DT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UPD_BY` int(10) UNSIGNED DEFAULT NULL,
  `UPD_DT` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `stock`
--

CREATE TABLE `stock` (
  `STOCK_ID` int(10) NOT NULL COMMENT 'Primary Key',
  `PROD_CODE` varchar(250) NOT NULL COMMENT 'Product Cood From sa_product',
  `RECEIVE_ID` int(10) DEFAULT NULL COMMENT 'Foregion key of receive_mst',
  `TRANSACTION_DATE` date NOT NULL COMMENT 'Every Stock Transaction Date',
  `OPENING_BAL` varchar(250) NOT NULL COMMENT 'Stock Opening Balance of this product',
  `RECEIVE_QTY` int(10) NOT NULL COMMENT 'IN every Receive transaction receive product Quantity',
  `ISSUE_QTY` int(10) NOT NULL COMMENT 'In Every Order issue this product quantity',
  `BATCH_NO` varchar(50) NOT NULL COMMENT 'This Product Batch No when receive this',
  `IS_ACTIVE` tinyint(1) DEFAULT '1' COMMENT 'It keeps boolean value representing Active/Inactive. e.g. 1 = Active, 0 = Inactive',
  `CRE_BY` int(10) UNSIGNED DEFAULT NULL,
  `CRE_DT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UPD_BY` int(10) UNSIGNED DEFAULT NULL,
  `UPD_DT` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `suppliery_paymentmst`
--

CREATE TABLE `suppliery_paymentmst` (
  `PAY_ID` int(10) NOT NULL COMMENT 'Primary Key',
  `PAY_DATE` date NOT NULL COMMENT 'paydate Date',
  `SUPP_CODE` varchar(100) NOT NULL COMMENT 'Supplier Code',
  `RECEIVE_ID` int(10) NOT NULL COMMENT 'Foregion Key receive_mst',
  `PAYMENT_RECEIPT_NO` varchar(10) NOT NULL COMMENT 'Pay number',
  `CASH_AMOUNT` int(10) NOT NULL COMMENT 'total pay amount',
  `PAY_MODE` varchar(10) NOT NULL COMMENT 'pay by',
  `IS_ACTIVE` tinyint(1) DEFAULT '1' COMMENT 'It keeps boolean value representing Active/Inactive. e.g. 1 = Active, 0 = Inactive',
  `CRE_BY` int(10) UNSIGNED DEFAULT NULL,
  `CRE_DT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UPD_BY` int(10) UNSIGNED DEFAULT NULL,
  `UPD_DT` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `supplier_info`
--

CREATE TABLE `supplier_info` (
  `SUPP_ID` int(10) NOT NULL COMMENT 'Primary Key',
  `SUPP_CODE` varchar(100) NOT NULL COMMENT 'Supplier Code',
  `SUPP_NAME` varchar(250) NOT NULL COMMENT 'Supplier Full Name',
  `ADDRESS` varchar(300) DEFAULT NULL COMMENT 'Supplier Address',
  `CONTACT_PERSON` varchar(250) DEFAULT NULL COMMENT 'Supplier Contact Person Name',
  `DESIGNATION` varchar(100) DEFAULT NULL COMMENT 'Contact Person Designation',
  `MOBILE` varchar(30) DEFAULT NULL COMMENT 'Contact Person Mobile',
  `EMAIL` varchar(50) DEFAULT NULL COMMENT 'Contact Person Email',
  `IS_ACTIVE` tinyint(1) DEFAULT '1' COMMENT 'It keeps boolean value representing Active/Inactive. e.g. 1 = Active, 0 = Inactive',
  `CRE_BY` int(10) UNSIGNED DEFAULT NULL,
  `CRE_DT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UPD_BY` int(10) UNSIGNED DEFAULT NULL,
  `UPD_DT` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `batch_info`
--
ALTER TABLE `batch_info`
  ADD PRIMARY KEY (`BATCH_ID`);

--
-- Indexes for table `customer_info`
--
ALTER TABLE `customer_info`
  ADD PRIMARY KEY (`CUST_ID`);

--
-- Indexes for table `invoice_chd`
--
ALTER TABLE `invoice_chd`
  ADD PRIMARY KEY (`INVOICE_CHD_ID`),
  ADD KEY `FK01_INVOICE_ID` (`INVOICE_ID`);

--
-- Indexes for table `invoice_mst`
--
ALTER TABLE `invoice_mst`
  ADD PRIMARY KEY (`INVOICE_ID`);

--
-- Indexes for table `money_collection`
--
ALTER TABLE `money_collection`
  ADD PRIMARY KEY (`COLL_NO`);

--
-- Indexes for table `order_chd`
--
ALTER TABLE `order_chd`
  ADD PRIMARY KEY (`ORDER_CHD_ID`),
  ADD KEY `FK01_ORDER_ID` (`ORDER_ID`);

--
-- Indexes for table `order_mst`
--
ALTER TABLE `order_mst`
  ADD PRIMARY KEY (`ORDER_ID`);

--
-- Indexes for table `receive_chd`
--
ALTER TABLE `receive_chd`
  ADD PRIMARY KEY (`RECEIVE_CHD_ID`),
  ADD KEY `FK01_RECEIVE_ID` (`RECEIVE_ID`);

--
-- Indexes for table `receive_mst`
--
ALTER TABLE `receive_mst`
  ADD PRIMARY KEY (`RECEIVE_ID`);

--
-- Indexes for table `sa_category`
--
ALTER TABLE `sa_category`
  ADD PRIMARY KEY (`CAT_ID`);

--
-- Indexes for table `sa_employee`
--
ALTER TABLE `sa_employee`
  ADD PRIMARY KEY (`EMP_ID`);

--
-- Indexes for table `sa_product`
--
ALTER TABLE `sa_product`
  ADD PRIMARY KEY (`PROD_ID`),
  ADD KEY `sa_product_fk01` (`CAT_ID`),
  ADD KEY `sa_product_fk02` (`SUBCAT_ID`);

--
-- Indexes for table `sa_sub_category`
--
ALTER TABLE `sa_sub_category`
  ADD PRIMARY KEY (`SUBCAT_ID`),
  ADD KEY `FK01_CAT_ID` (`CAT_ID`);

--
-- Indexes for table `sa_users`
--
ALTER TABLE `sa_users`
  ADD PRIMARY KEY (`USER_ID`),
  ADD KEY `FK01_EMP_ID` (`EMP_ID`);

--
-- Indexes for table `stock`
--
ALTER TABLE `stock`
  ADD PRIMARY KEY (`STOCK_ID`);

--
-- Indexes for table `suppliery_paymentmst`
--
ALTER TABLE `suppliery_paymentmst`
  ADD PRIMARY KEY (`PAY_ID`);

--
-- Indexes for table `supplier_info`
--
ALTER TABLE `supplier_info`
  ADD PRIMARY KEY (`SUPP_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `batch_info`
--
ALTER TABLE `batch_info`
  MODIFY `BATCH_ID` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key';
--
-- AUTO_INCREMENT for table `customer_info`
--
ALTER TABLE `customer_info`
  MODIFY `CUST_ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key';
--
-- AUTO_INCREMENT for table `invoice_chd`
--
ALTER TABLE `invoice_chd`
  MODIFY `INVOICE_CHD_ID` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key';
--
-- AUTO_INCREMENT for table `invoice_mst`
--
ALTER TABLE `invoice_mst`
  MODIFY `INVOICE_ID` int(20) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key';
--
-- AUTO_INCREMENT for table `money_collection`
--
ALTER TABLE `money_collection`
  MODIFY `COLL_NO` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key';
--
-- AUTO_INCREMENT for table `order_chd`
--
ALTER TABLE `order_chd`
  MODIFY `ORDER_CHD_ID` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key';
--
-- AUTO_INCREMENT for table `order_mst`
--
ALTER TABLE `order_mst`
  MODIFY `ORDER_ID` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key';
--
-- AUTO_INCREMENT for table `receive_chd`
--
ALTER TABLE `receive_chd`
  MODIFY `RECEIVE_CHD_ID` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key';
--
-- AUTO_INCREMENT for table `receive_mst`
--
ALTER TABLE `receive_mst`
  MODIFY `RECEIVE_ID` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key';
--
-- AUTO_INCREMENT for table `sa_category`
--
ALTER TABLE `sa_category`
  MODIFY `CAT_ID` int(20) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key';
--
-- AUTO_INCREMENT for table `sa_employee`
--
ALTER TABLE `sa_employee`
  MODIFY `EMP_ID` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key';
--
-- AUTO_INCREMENT for table `sa_product`
--
ALTER TABLE `sa_product`
  MODIFY `PROD_ID` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key';
--
-- AUTO_INCREMENT for table `sa_sub_category`
--
ALTER TABLE `sa_sub_category`
  MODIFY `SUBCAT_ID` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key';
--
-- AUTO_INCREMENT for table `sa_users`
--
ALTER TABLE `sa_users`
  MODIFY `USER_ID` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key';
--
-- AUTO_INCREMENT for table `stock`
--
ALTER TABLE `stock`
  MODIFY `STOCK_ID` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key';
--
-- AUTO_INCREMENT for table `suppliery_paymentmst`
--
ALTER TABLE `suppliery_paymentmst`
  MODIFY `PAY_ID` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key';
--
-- AUTO_INCREMENT for table `supplier_info`
--
ALTER TABLE `supplier_info`
  MODIFY `SUPP_ID` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key';
--
-- Constraints for dumped tables
--

--
-- Constraints for table `invoice_chd`
--
ALTER TABLE `invoice_chd`
  ADD CONSTRAINT `FK01_INVOICE_ID` FOREIGN KEY (`INVOICE_ID`) REFERENCES `invoice_mst` (`INVOICE_ID`);

--
-- Constraints for table `order_chd`
--
ALTER TABLE `order_chd`
  ADD CONSTRAINT `FK01_ORDER_ID` FOREIGN KEY (`ORDER_ID`) REFERENCES `order_mst` (`ORDER_ID`);

--
-- Constraints for table `receive_chd`
--
ALTER TABLE `receive_chd`
  ADD CONSTRAINT `FK01_RECEIVE_ID` FOREIGN KEY (`RECEIVE_ID`) REFERENCES `receive_mst` (`RECEIVE_ID`);

--
-- Constraints for table `sa_product`
--
ALTER TABLE `sa_product`
  ADD CONSTRAINT `sa_product_fk01` FOREIGN KEY (`CAT_ID`) REFERENCES `sa_category` (`CAT_ID`),
  ADD CONSTRAINT `sa_product_fk02` FOREIGN KEY (`SUBCAT_ID`) REFERENCES `sa_sub_category` (`SUBCAT_ID`);

--
-- Constraints for table `sa_sub_category`
--
ALTER TABLE `sa_sub_category`
  ADD CONSTRAINT `FK01_CAT_ID` FOREIGN KEY (`CAT_ID`) REFERENCES `sa_category` (`CAT_ID`);

--
-- Constraints for table `sa_users`
--
ALTER TABLE `sa_users`
  ADD CONSTRAINT `FK01_EMP_ID` FOREIGN KEY (`EMP_ID`) REFERENCES `sa_employee` (`EMP_ID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
