</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> <?php echo date("Y"); ?>
    </div>
    <strong>Copyright &copy; <?php echo date("Y"); ?> <a href="#">abc</a>.</strong> All rights
    reserved.
</footer>