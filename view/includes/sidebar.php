<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="../resource/img/avater.png" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>BMS Admin</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                    <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i
                                class="fa fa-search"></i>
                    </button>
                </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li><a href="dashboard.php"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-book"></i>
                    <span>All Setup</span>
                    <span class="pull-right-container">
                        <span class="label label-primary pull-right">4</span>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="outletAdd.php"><i class="fa fa-circle-o"></i>  Outlet</a></li>
                    <li><a href="supplier.php"><i class="fa fa-circle-o"></i> Sub-Category</a></li>
                    <li><a href="supplier.php"><i class="fa fa-circle-o"></i> Supplier</a></li>
                    <li><a href="product.php"><i class="fa fa-circle-o"></i> Product</a></li>
                    <li><a href="supplier.php"><i class="fa fa-circle-o"></i> Employee</a></li>
                </ul>
            </li>
            <li><a href="dashboard.php"><i class="fa fa-users"></i> <span>Employee</span></a></li>
            <li><a href="purchase.php"><i class="fa fa-archive"></i> <span>Product Purchase</span></a></li>
            <li><a href="receive.php"><i class="fa fa-shopping-cart"></i> <span>Product Receive</span></a></li>
            <li><a href="sales.php"><i class="fa fa-money"></i> <span>Product Sales</span></a></li>
            <li><a href="sales_return.php"><i class="fa fa-money"></i> <span>Sales Return</span></a></li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-files-o"></i>
                    <span>Report</span>
                    <span class="pull-right-container">
                        <span class="label label-primary pull-right">4</span>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="category.php"><i class="fa fa-circle-o"></i> Stock Report</a></li>
                    <li><a href="pages/layout/boxed.html"><i class="fa fa-circle-o"></i> Product Wise Sales Report</a>
                    </li>
                    <li><a href="pages/layout/fixed.html"><i class="fa fa-circle-o"></i>Daily Sales Report</a></li>
                    <li><a href="pages/layout/collapsed-sidebar.html"><i class="fa fa-circle-o"></i> Customer Ledger</a>
                    </li>
                </ul>
            </li>
            <!--<li><a href="#"><i class="fa fa-book"></i> <span>Documentation</span></a></li>-->
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>