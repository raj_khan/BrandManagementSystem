<!DOCTYPE html>
<html>
<?php include("includes/head.php"); ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <?php include("includes/header.php"); ?>
    <?php include("includes/sidebar.php"); ?>
    <?php include("includes/breadcrumb.php"); ?>

    <!-- Main content -->
    <!-- Main content -->
    <div class="row">
        <!-- left column -->
        <div class="col-md-6 col-md-offset-3">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border text-center">
                    <h3 class="box-title text-primary heading_one ">Outlet Information Entry Panel</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form">
                    <div class="box-body">
                        <!--Outlet Id-->
                        <div class="form-group">
                            <label for="outletId">Outlet ID</label>
                            <input type="number" name="outletId" placeholder="01" readonly>
                        </div>


                        <!--Outlet Number-->
                        <div class="form-group">
                            <label for="exampleInputEmail1">Outlet Number</label>
                            <input type="text" class="form-control" id="outletNumber" name="outletNumber"
                                   placeholder="Outlet Name">
                        </div>

                        <!--Outlet Name-->
                        <div class="form-group">
                            <label for="exampleInputEmail1">Outlet Name</label>
                            <input type="text" class="form-control" id="outletName" name="outletName"
                                   placeholder="Outlet Name">
                        </div>

                        <!--Outlet Address-->
                        <div class="form-group">
                            <label for="exampleInputEmail1">Address</label>
                            <textarea class="form-control" rows="1" cols="5" name="outletAddress"
                                      placeholder="Outlet Address"></textarea>
                        </div>

                        <!--Proprietor Name-->
                        <div class="form-group">
                            <label for="exampleInputEmail1">Proprietor Name</label>
                            <input type="text" class="form-control" id="proprietorName" name="proprietorName"
                                   placeholder="Proprietor Name">
                        </div>

                        <!--Proprietor Contact-->
                        <div class="form-group">
                            <label for="exampleInputEmail1">Proprietor Contact</label>
                            <input type="number" class="form-control" id="proprietorContact" name="proprietorContact"
                                   placeholder="Proprietor Contact">
                        </div>

                        <!--Proprietor Email-->
                        <div class="form-group">
                            <label for="exampleInputEmail1">Proprietor Email</label>
                            <input type="email" class="form-control" id="proprietorEmail" name="proprietorEmail"
                                   placeholder="Proprietor Email" autocomplete="">
                        </div>


                        <!--Company-->
                        <div class="form-group">
                            <label for="company">Company</label>
                            <select class="form-control" name="company">
                                <option value="">-- Select Company --</option>
                                <option value="">-- Demo Company --</option>
                                <option value="">-- Demo Company --</option>
                            </select>
                        </div>


                        <!--Category-->
                        <div class="form-group">
                            <label for="category">Category</label>
                            <select class="form-control" name="category">
                                <option value="">-- Select Category --</option>
                                <option value="">-- Demo Category --</option>
                                <option value="">-- Demo Category --</option>
                            </select>
                        </div>


                        <!--Products-->
                        <div class="form-group">
                            <label for="products">Products</label>
                            <select class="form-control" name="products">
                                <option value="">-- Select Products --</option>
                                <option value="">-- Demo Products --</option>
                                <option value="">-- Demo Products --</option>
                            </select>
                        </div>


                        <!--Type-->
                        <div class="form-group">
                            <label for="type">Type</label>
                            <select class="form-control" name="type">
                                <option value="">-- Select Type --</option>
                                <option value="">-- Demo Type --</option>
                                <option value="">-- Demo Type --</option>
                            </select>
                        </div>


                        <!--Size Horizontaly-->
                        <div class="form-group">
                            <label for="SizeHorizontal">Size Horizontal</label>
                            <input type="text" class="form-control" id="sizeHorizontal" name="SizeHorizontal"
                                   placeholder="Size Horizontal">
                        </div>


                        <!--Size Vertically-->
                        <div class="form-group">
                            <label for="SizeVertically">Size Vertically</label>
                            <input type="text" class="form-control" id="sizeVertically" name="SizeVertically"
                                   placeholder="Size Vertically">
                        </div>


                        <!--Grade-->
                        <div class="form-group">
                            <label for="grade">Grade</label>
                            <select class="form-control" name="grade">
                                <option value="">-- Select Grade --</option>
                                <option value="">-- Demo Grade --</option>
                                <option value="">-- Demo Grade --</option>
                            </select>
                        </div>

                        <!--Fridge-->
                        <div class="form-group">
                            <label for="fridge">Fridge</label>
                            <select class="form-control" name="fridge">
                                <option value="">-- Select Fridge --</option>
                                <option value="">-- Demo Fridge --</option>
                                <option value="">-- Demo Fridge --</option>
                            </select>
                        </div>


                        <!--Signboard-->
                        <div class="form-group">
                            <label for="signboard">Signboard</label>
                            <select class="form-control" name="signboard">
                                <option value="">-- Select Signboard --</option>
                                <option value="">-- Demo Signboard --</option>
                                <option value="">-- Demo Signboard --</option>
                            </select>
                        </div>


                        <!--Rack/Unit-->
                        <div class="form-group">
                            <label for="unit">Rack/Unit</label>
                            <select class="form-control" name="unit">
                                <option value="">-- Select Rack/Unit --</option>
                                <option value="">-- Demo Rack/Unit --</option>
                                <option value="">-- Demo Rack/Unit --</option>
                            </select>
                        </div>


                        <!--Division-->
                        <div class="form-group">
                            <label for="division">Division</label>
                            <select class="form-control" name="division">
                                <option value="">-- Select Division --</option>
                                <option value="">-- Demo Division --</option>
                                <option value="">-- Demo Division --</option>
                            </select>
                        </div>

                        <!--country-->
                        <div class="form-group">
                            <label for="country">Country</label>
                            <select class="form-control" name="country">
                                <option value="">-- Select country --</option>
                                <option value="">-- Demo country --</option>
                                <option value="">-- Demo country --</option>
                            </select>
                        </div>

                        <!--Zone-->
                        <div class="form-group">
                            <label for="zone">Zone</label>
                            <select class="form-control" name="zone">
                                <option value="">-- Select Zone --</option>
                                <option value="">-- Demo Zone --</option>
                                <option value="">-- Demo Zone --</option>
                            </select>
                        </div>


                        <!--Base-->
                        <div class="form-group">
                            <label for="base">Base</label>
                            <select class="form-control" name="base">
                                <option value="">-- Select Base --</option>
                                <option value="">-- Demo Base --</option>
                                <option value="">-- Demo Base --</option>
                            </select>
                        </div>


                        <!--User-->
                        <div class="form-group">
                            <label for="user">User</label>
                            <select class="form-control" name="user">
                                <option value="">-- Select User --</option>
                                <option value="">-- Demo User --</option>
                                <option value="">-- Demo User --</option>
                            </select>
                        </div>


                        <!--Route-->
                        <div class="form-group">
                            <label for="route">Route</label>
                            <select class="form-control" name="route">
                                <option value="">-- Select Route --</option>
                                <option value="">-- Demo Route --</option>
                                <option value="">-- Demo Route --</option>
                            </select>
                        </div>


                        <!--Remarks-->
                        <div class="form-group">
                            <label for="remarks">Remarks</label>
                            <textarea class="form-control" rows="1" cols="5" name="remarks"
                                      placeholder="Remarks"></textarea>
                        </div>


                        <!--Status-->
                        <div class="form-group">
                            <label for="remarks">Status: </label>
                            <input type="radio" name="status" value="Y">
                            <label for="remarks">Yes</label>


                            <input type="radio" name="status" value="N">
                            <label for="remarks">No</label>
                        </div>


                        <!--Status-->
                        <div class="form-group">
                            <label for="route">Status</label>
                            <select class="form-control" name="status">
                                <option value="">YES</option>
                                <option value="">NO</option>
                            </select>
                        </div>


                        <!--Image-->
                        <div class="form-group">
                            <label for="image">Image</label>
                            <input type="file">
                        </div>


                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
            <!-- /.box -->
        </div>
        <!--/.col (left) -->


    </div>
    <!-- /.row -->


    <div class="row">
        <!-- left column -->

        <!-- right column -->
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title text-primary heading_one">Outlet Info: </h3>
                    <!--                    <a href=""><abbr title="EXCEL"><i class="fa fa-file-excel-o file_open_icon"></i></abbr></a>-->
                    <!--                    <a href=""><abbr title="PDF"><i class="fa fa-file-pdf-o file_open_icon"></i></abbr></a>-->
                    <!--                    <a href=""><abbr title="CVS"><i class="fa fa-file-o file_open_icon"></i></abbr></a>-->
                    <!--                    <a href=""><abbr title="PRINT"><i class="fa fa-print file_open_icon"></i></abbr></a>-->

                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <table id="outletInfo" class="table table-responsive" style="width:100%">
                    <thead>
                    <tr class="bg-primary">
                        <td>SL</td>
                        <td>OL NO.</td>
                        <td>OL Name</td>
                        <td>Addr.</td>
                        <td>Name</td>
                        <td>Contact</td>
                        <td>Email</td>
                        <td>Comp.</td>
                        <td>Cat</td>
                        <td>Prod.</td>
                        <td>Type</td>
                        <td>S-H</td>
                        <td>S-V</td>
                        <td>T S</td>
                        <td>G</td>
                        <td>Fridge</td>
                        <td>S Board</td>
                        <td>Unit</td>
                        <td>Country</td>
                        <td>Div</td>
                        <td>Zone</td>
                        <td>Base</td>
                        <td>User</td>
                        <td>Route</td>
                        <td>Img</td>
                        <td>Action</td>
                    </tr>
                    </thead>
                    <tbody>

                    <!--Single Data-->
                    <tr class="text-center">
                        <th>01</th>
                        <th>B012H</th>
                        <th>MA Traders</th>
                        <th>South Badda</th>
                        <th>AK Ullash</th>
                        <th>01752046668</th>
                        <th>ak.407@gmail.com</th>
                        <th>NAL</th>
                        <th>Category</th>
                        <th>Biscuit</th>
                        <th>Type Energy</th>
                        <th>1.5 inch</th>
                        <th>2.5 inch</th>
                        <th>4 sft</th>
                        <th>A Grade</th>
                        <th>5.5 Fridge</th>
                        <th>SignBoard</th>
                        <th>Unit</th>
                        <th>Bangladesh</th>
                        <th>Uttara</th>
                        <th>Zone</th>
                        <th>Base</th>
                        <th>User</th>
                        <th>Route</th>
                        <th>
                            <a href="img/test.jpg" data-lightbox="outletImage" data-title="Title"> <img
                                        class="outlet_image" src="img/test.jpg"></a>
                            <a href="img/test.jpg" data-lightbox="outletImage" data-title="Title"> <img
                                        class="outlet_image" src="img/test.jpg"></a>

                        </th>
                        <th>
                            <a href="#"><i class="fa fa-pencil btn-info"
                                           style="padding: 5px; border-radius: 5px"></i></a>
                            <a href="#"><i class="fa fa-remove btn-danger" style="padding: 5px; border-radius: 5px"></i></a>
                        </th>
                    </tr>
                    <!--End Data-->


                    <!--Single Data-->
                    <tr class="text-center">
                        <th>01</th>
                        <th>B012H</th>
                        <th>MA Traders</th>
                        <th>South Badda</th>
                        <th>AK Ullash</th>
                        <th>01752046668</th>
                        <th>ak.407@gmail.com</th>
                        <th>NAL</th>
                        <th>Category</th>
                        <th>Biscuit</th>
                        <th>Type Energy</th>
                        <th>1.5 inch</th>
                        <th>2.5 inch</th>
                        <th>4 sft</th>
                        <th>A Grade</th>
                        <th>5.5 Fridge</th>
                        <th>SignBoard</th>
                        <th>Unit</th>
                        <th>Bangladesh</th>
                        <th>Uttara</th>
                        <th>Zone</th>
                        <th>Base</th>
                        <th>User</th>
                        <th>Route</th>
                        <th>
                            <a href="img/test.jpg" data-lightbox="outletImage" data-title="Title"> <img
                                        class="outlet_image" src="img/test.jpg"></a>
                            <a href="img/test.jpg" data-lightbox="outletImage" data-title="Title"> <img
                                        class="outlet_image" src="img/test.jpg"></a>

                        </th>
                        <th>
                            <a href="#"><i class="fa fa-pencil btn-info"
                                           style="padding: 5px; border-radius: 5px"></i></a>
                            <a href="#"><i class="fa fa-remove btn-danger" style="padding: 5px; border-radius: 5px"></i></a>
                        </th>
                    </tr>
                    <!--End Data-->


                    <!--Single Data-->
                    <tr class="text-center">
                        <th>01</th>
                        <th>B012H</th>
                        <th>MA Traders</th>
                        <th>South Badda</th>
                        <th>AK Ullash</th>
                        <th>01752046668</th>
                        <th>ak.407@gmail.com</th>
                        <th>NAL</th>
                        <th>Category</th>
                        <th>Biscuit</th>
                        <th>Type Energy</th>
                        <th>1.5 inch</th>
                        <th>2.5 inch</th>
                        <th>4 sft</th>
                        <th>A Grade</th>
                        <th>5.5 Fridge</th>
                        <th>SignBoard</th>
                        <th>Unit</th>
                        <th>Bangladesh</th>
                        <th>Uttara</th>
                        <th>Zone</th>
                        <th>Base</th>
                        <th>User</th>
                        <th>Route</th>
                        <th>
                            <a href="img/test.jpg" data-lightbox="outletImage" data-title="Title"> <img
                                        class="outlet_image" src="img/test.jpg"></a>
                            <a href="img/test.jpg" data-lightbox="outletImage" data-title="Title"> <img
                                        class="outlet_image" src="img/test.jpg"></a>

                        </th>
                        <th>
                            <a href="#"><i class="fa fa-pencil btn-info"
                                           style="padding: 5px; border-radius: 5px"></i></a>
                            <a href="#"><i class="fa fa-remove btn-danger" style="padding: 5px; border-radius: 5px"></i></a>
                        </th>
                    </tr>
                    <!--End Data-->


                    <!--Single Data-->
                    <tr class="text-center">
                        <th>01</th>
                        <th>B012H</th>
                        <th>MA Traders</th>
                        <th>South Badda</th>
                        <th>AK Ullash</th>
                        <th>01752046668</th>
                        <th>ak.407@gmail.com</th>
                        <th>NAL</th>
                        <th>Category</th>
                        <th>Biscuit</th>
                        <th>Type Energy</th>
                        <th>1.5 inch</th>
                        <th>2.5 inch</th>
                        <th>4 sft</th>
                        <th>A Grade</th>
                        <th>5.5 Fridge</th>
                        <th>SignBoard</th>
                        <th>Unit</th>
                        <th>Bangladesh</th>
                        <th>Uttara</th>
                        <th>Zone</th>
                        <th>Base</th>
                        <th>User</th>
                        <th>Route</th>
                        <th>
                            <a href="img/test.jpg" data-lightbox="outletImage" data-title="Title"> <img
                                        class="outlet_image" src="img/test.jpg"></a>
                            <a href="img/test.jpg" data-lightbox="outletImage" data-title="Title"> <img
                                        class="outlet_image" src="img/test.jpg"></a>

                        </th>
                        <th>
                            <a href="#"><i class="fa fa-pencil btn-info"
                                           style="padding: 5px; border-radius: 5px"></i></a>
                            <a href="#"><i class="fa fa-remove btn-danger" style="padding: 5px; border-radius: 5px"></i></a>
                        </th>
                    </tr>
                    <!--End Data-->


                    <!--Single Data-->
                    <tr class="text-center">
                        <th>01</th>
                        <th>B012H</th>
                        <th>MA Traders</th>
                        <th>South Badda</th>
                        <th>AK Ullash</th>
                        <th>01752046668</th>
                        <th>ak.407@gmail.com</th>
                        <th>NAL</th>
                        <th>Category</th>
                        <th>Biscuit</th>
                        <th>Type Energy</th>
                        <th>1.5 inch</th>
                        <th>2.5 inch</th>
                        <th>4 sft</th>
                        <th>A Grade</th>
                        <th>5.5 Fridge</th>
                        <th>SignBoard</th>
                        <th>Unit</th>
                        <th>Bangladesh</th>
                        <th>Uttara</th>
                        <th>Zone</th>
                        <th>Base</th>
                        <th>User</th>
                        <th>Route</th>
                        <th>
                            <a href="img/test.jpg" data-lightbox="outletImage" data-title="Title"> <img
                                        class="outlet_image" src="img/test.jpg"></a>
                            <a href="img/test.jpg" data-lightbox="outletImage" data-title="Title"> <img
                                        class="outlet_image" src="img/test.jpg"></a>

                        </th>
                        <th>
                            <a href="#"><i class="fa fa-pencil btn-info"
                                           style="padding: 5px; border-radius: 5px"></i></a>
                            <a href="#"><i class="fa fa-remove btn-danger" style="padding: 5px; border-radius: 5px"></i></a>
                        </th>
                    </tr>
                    <!--End Data-->


                    <!--Single Data-->
                    <tr class="text-center">
                        <th>01</th>
                        <th>B012H</th>
                        <th>MA Traders</th>
                        <th>South Badda</th>
                        <th>AK Ullash</th>
                        <th>01752046668</th>
                        <th>ak.407@gmail.com</th>
                        <th>NAL</th>
                        <th>Category</th>
                        <th>Biscuit</th>
                        <th>Type Energy</th>
                        <th>1.5 inch</th>
                        <th>2.5 inch</th>
                        <th>4 sft</th>
                        <th>A Grade</th>
                        <th>5.5 Fridge</th>
                        <th>SignBoard</th>
                        <th>Unit</th>
                        <th>Bangladesh</th>
                        <th>Uttara</th>
                        <th>Zone</th>
                        <th>Base</th>
                        <th>User</th>
                        <th>Route</th>
                        <th>
                            <a href="img/test.jpg" data-lightbox="outletImage" data-title="Title"> <img
                                        class="outlet_image" src="img/test.jpg"></a>
                            <a href="img/test.jpg" data-lightbox="outletImage" data-title="Title"> <img
                                        class="outlet_image" src="img/test.jpg"></a>

                        </th>
                        <th>
                            <a href="#"><i class="fa fa-pencil btn-info"
                                           style="padding: 5px; border-radius: 5px"></i></a>
                            <a href="#"><i class="fa fa-remove btn-danger" style="padding: 5px; border-radius: 5px"></i></a>
                        </th>
                    </tr>
                    <!--End Data-->


                    <!--Single Data-->
                    <tr class="text-center">
                        <th>01</th>
                        <th>B012H</th>
                        <th>MA Traders</th>
                        <th>South Badda</th>
                        <th>AK Ullash</th>
                        <th>01752046668</th>
                        <th>ak.407@gmail.com</th>
                        <th>NAL</th>
                        <th>Category</th>
                        <th>Biscuit</th>
                        <th>Type Energy</th>
                        <th>1.5 inch</th>
                        <th>2.5 inch</th>
                        <th>4 sft</th>
                        <th>A Grade</th>
                        <th>5.5 Fridge</th>
                        <th>SignBoard</th>
                        <th>Unit</th>
                        <th>Bangladesh</th>
                        <th>Uttara</th>
                        <th>Zone</th>
                        <th>Base</th>
                        <th>User</th>
                        <th>Route</th>
                        <th>
                            <a href="img/test.jpg" data-lightbox="outletImage" data-title="Title"> <img
                                        class="outlet_image" src="img/test.jpg"></a>
                            <a href="img/test.jpg" data-lightbox="outletImage" data-title="Title"> <img
                                        class="outlet_image" src="img/test.jpg"></a>

                        </th>
                        <th>
                            <a href="#"><i class="fa fa-pencil btn-info"
                                           style="padding: 5px; border-radius: 5px"></i></a>
                            <a href="#"><i class="fa fa-remove btn-danger" style="padding: 5px; border-radius: 5px"></i></a>
                        </th>
                    </tr>
                    <!--End Data-->


                    <!--Single Data-->
                    <tr class="text-center">
                        <th>01</th>
                        <th>B012H</th>
                        <th>MA Traders</th>
                        <th>South Badda</th>
                        <th>AK Ullash</th>
                        <th>01752046668</th>
                        <th>ak.407@gmail.com</th>
                        <th>NAL</th>
                        <th>Category</th>
                        <th>Biscuit</th>
                        <th>Type Energy</th>
                        <th>1.5 inch</th>
                        <th>2.5 inch</th>
                        <th>4 sft</th>
                        <th>A Grade</th>
                        <th>5.5 Fridge</th>
                        <th>SignBoard</th>
                        <th>Unit</th>
                        <th>Bangladesh</th>
                        <th>Uttara</th>
                        <th>Zone</th>
                        <th>Base</th>
                        <th>User</th>
                        <th>Route</th>
                        <th>
                            <a href="img/test.jpg" data-lightbox="outletImage" data-title="Title"> <img
                                        class="outlet_image" src="img/test.jpg"></a>
                            <a href="img/test.jpg" data-lightbox="outletImage" data-title="Title"> <img
                                        class="outlet_image" src="img/test.jpg"></a>

                        </th>
                        <th>
                            <a href="#"><i class="fa fa-pencil btn-info"
                                           style="padding: 5px; border-radius: 5px"></i></a>
                            <a href="#"><i class="fa fa-remove btn-danger" style="padding: 5px; border-radius: 5px"></i></a>
                        </th>
                    </tr>
                    <!--End Data-->


                    <!--Single Data-->
                    <tr class="text-center">
                        <th>01</th>
                        <th>B012H</th>
                        <th>MA Traders</th>
                        <th>South Badda</th>
                        <th>AK Ullash</th>
                        <th>01752046668</th>
                        <th>ak.407@gmail.com</th>
                        <th>NAL</th>
                        <th>Category</th>
                        <th>Biscuit</th>
                        <th>Type Energy</th>
                        <th>1.5 inch</th>
                        <th>2.5 inch</th>
                        <th>4 sft</th>
                        <th>A Grade</th>
                        <th>5.5 Fridge</th>
                        <th>SignBoard</th>
                        <th>Unit</th>
                        <th>Bangladesh</th>
                        <th>Uttara</th>
                        <th>Zone</th>
                        <th>Base</th>
                        <th>User</th>
                        <th>Route</th>
                        <th>
                            <a href="img/test.jpg" data-lightbox="outletImage" data-title="Title"> <img
                                        class="outlet_image" src="img/test.jpg"></a>
                            <a href="img/test.jpg" data-lightbox="outletImage" data-title="Title"> <img
                                        class="outlet_image" src="img/test.jpg"></a>

                        </th>
                        <th>
                            <a href="#"><i class="fa fa-pencil btn-info"
                                           style="padding: 5px; border-radius: 5px"></i></a>
                            <a href="#"><i class="fa fa-remove btn-danger" style="padding: 5px; border-radius: 5px"></i></a>
                        </th>
                    </tr>
                    <!--End Data-->


                    <!--Single Data-->
                    <tr class="text-center">
                        <th>01</th>
                        <th>B012H</th>
                        <th>MA Traders</th>
                        <th>South Badda</th>
                        <th>AK Ullash</th>
                        <th>01752046668</th>
                        <th>ak.407@gmail.com</th>
                        <th>NAL</th>
                        <th>Category</th>
                        <th>Biscuit</th>
                        <th>Type Energy</th>
                        <th>1.5 inch</th>
                        <th>2.5 inch</th>
                        <th>4 sft</th>
                        <th>A Grade</th>
                        <th>5.5 Fridge</th>
                        <th>SignBoard</th>
                        <th>Unit</th>
                        <th>Bangladesh</th>
                        <th>Uttara</th>
                        <th>Zone</th>
                        <th>Base</th>
                        <th>User</th>
                        <th>Route</th>
                        <th>
                            <a href="img/test.jpg" data-lightbox="outletImage" data-title="Title"> <img
                                        class="outlet_image" src="img/test.jpg"></a>
                            <a href="img/test.jpg" data-lightbox="outletImage" data-title="Title"> <img
                                        class="outlet_image" src="img/test.jpg"></a>

                        </th>
                        <th>
                            <a href="#"><i class="fa fa-pencil btn-info"
                                           style="padding: 5px; border-radius: 5px"></i></a>
                            <a href="#"><i class="fa fa-remove btn-danger" style="padding: 5px; border-radius: 5px"></i></a>
                        </th>
                    </tr>
                    <!--End Data-->


                    <!--Single Data-->
                    <tr class="text-center">
                        <th>01</th>
                        <th>B012H</th>
                        <th>MA Traders</th>
                        <th>South Badda</th>
                        <th>AK Ullash</th>
                        <th>01752046668</th>
                        <th>ak.407@gmail.com</th>
                        <th>NAL</th>
                        <th>Category</th>
                        <th>Biscuit</th>
                        <th>Type Energy</th>
                        <th>1.5 inch</th>
                        <th>2.5 inch</th>
                        <th>4 sft</th>
                        <th>A Grade</th>
                        <th>5.5 Fridge</th>
                        <th>SignBoard</th>
                        <th>Unit</th>
                        <th>Bangladesh</th>
                        <th>Uttara</th>
                        <th>Zone</th>
                        <th>Base</th>
                        <th>User</th>
                        <th>Route</th>
                        <th>
                            <a href="img/test.jpg" data-lightbox="outletImage" data-title="Title"> <img
                                        class="outlet_image" src="img/test.jpg"></a>
                            <a href="img/test.jpg" data-lightbox="outletImage" data-title="Title"> <img
                                        class="outlet_image" src="img/test.jpg"></a>

                        </th>
                        <th>
                            <a href="#"><i class="fa fa-pencil btn-info"
                                           style="padding: 5px; border-radius: 5px"></i></a>
                            <a href="#"><i class="fa fa-remove btn-danger" style="padding: 5px; border-radius: 5px"></i></a>
                        </th>
                    </tr>
                    <!--End Data-->


                    <!--Single Data-->
                    <tr class="text-center">
                        <th>01</th>
                        <th>B012H</th>
                        <th>MA Traders</th>
                        <th>South Badda</th>
                        <th>AK Ullash</th>
                        <th>01752046668</th>
                        <th>ak.407@gmail.com</th>
                        <th>NAL</th>
                        <th>Category</th>
                        <th>Biscuit</th>
                        <th>Type Energy</th>
                        <th>1.5 inch</th>
                        <th>2.5 inch</th>
                        <th>4 sft</th>
                        <th>A Grade</th>
                        <th>5.5 Fridge</th>
                        <th>SignBoard</th>
                        <th>Unit</th>
                        <th>Bangladesh</th>
                        <th>Uttara</th>
                        <th>Zone</th>
                        <th>Base</th>
                        <th>User</th>
                        <th>Route</th>
                        <th>
                            <a href="img/test.jpg" data-lightbox="outletImage" data-title="Title"> <img
                                        class="outlet_image" src="img/test.jpg"></a>
                            <a href="img/test.jpg" data-lightbox="outletImage" data-title="Title"> <img
                                        class="outlet_image" src="img/test.jpg"></a>

                        </th>
                        <th>
                            <a href="#"><i class="fa fa-pencil btn-info"
                                           style="padding: 5px; border-radius: 5px"></i></a>
                            <a href="#"><i class="fa fa-remove btn-danger" style="padding: 5px; border-radius: 5px"></i></a>
                        </th>
                    </tr>
                    <!--End Data-->


                    <!--Single Data-->
                    <tr class="text-center">
                        <th>01</th>
                        <th>B012H</th>
                        <th>MA Traders</th>
                        <th>South Badda</th>
                        <th>AK Ullash</th>
                        <th>01752046668</th>
                        <th>ak.407@gmail.com</th>
                        <th>NAL</th>
                        <th>Category</th>
                        <th>Biscuit</th>
                        <th>Type Energy</th>
                        <th>1.5 inch</th>
                        <th>2.5 inch</th>
                        <th>4 sft</th>
                        <th>A Grade</th>
                        <th>5.5 Fridge</th>
                        <th>SignBoard</th>
                        <th>Unit</th>
                        <th>Bangladesh</th>
                        <th>Uttara</th>
                        <th>Zone</th>
                        <th>Base</th>
                        <th>User</th>
                        <th>Route</th>
                        <th>
                            <a href="img/test.jpg" data-lightbox="outletImage" data-title="Title"> <img
                                        class="outlet_image" src="img/test.jpg"></a>
                            <a href="img/test.jpg" data-lightbox="outletImage" data-title="Title"> <img
                                        class="outlet_image" src="img/test.jpg"></a>

                        </th>
                        <th>
                            <a href="#"><i class="fa fa-pencil btn-info"
                                           style="padding: 5px; border-radius: 5px"></i></a>
                            <a href="#"><i class="fa fa-remove btn-danger" style="padding: 5px; border-radius: 5px"></i></a>
                        </th>
                    </tr>
                    <!--End Data-->


                    <!--Single Data-->
                    <tr class="text-center">
                        <th>01</th>
                        <th>B012H</th>
                        <th>MA Traders</th>
                        <th>South Badda</th>
                        <th>AK Ullash</th>
                        <th>01752046668</th>
                        <th>ak.407@gmail.com</th>
                        <th>NAL</th>
                        <th>Category</th>
                        <th>Biscuit</th>
                        <th>Type Energy</th>
                        <th>1.5 inch</th>
                        <th>2.5 inch</th>
                        <th>4 sft</th>
                        <th>A Grade</th>
                        <th>5.5 Fridge</th>
                        <th>SignBoard</th>
                        <th>Unit</th>
                        <th>Bangladesh</th>
                        <th>Uttara</th>
                        <th>Zone</th>
                        <th>Base</th>
                        <th>User</th>
                        <th>Route</th>
                        <th>
                            <a href="img/test.jpg" data-lightbox="outletImage" data-title="Title"> <img
                                        class="outlet_image" src="img/test.jpg"></a>
                            <a href="img/test.jpg" data-lightbox="outletImage" data-title="Title"> <img
                                        class="outlet_image" src="img/test.jpg"></a>

                        </th>
                        <th>
                            <a href="#"><i class="fa fa-pencil btn-info"
                                           style="padding: 5px; border-radius: 5px"></i></a>
                            <a href="#"><i class="fa fa-remove btn-danger" style="padding: 5px; border-radius: 5px"></i></a>
                        </th>
                    </tr>
                    <!--End Data-->


                    <!--Single Data-->
                    <tr class="text-center">
                        <th>01</th>
                        <th>B012H</th>
                        <th>MA Traders</th>
                        <th>South Badda</th>
                        <th>AK Ullash</th>
                        <th>01752046668</th>
                        <th>ak.407@gmail.com</th>
                        <th>NAL</th>
                        <th>Category</th>
                        <th>Biscuit</th>
                        <th>Type Energy</th>
                        <th>1.5 inch</th>
                        <th>2.5 inch</th>
                        <th>4 sft</th>
                        <th>A Grade</th>
                        <th>5.5 Fridge</th>
                        <th>SignBoard</th>
                        <th>Unit</th>
                        <th>Bangladesh</th>
                        <th>Uttara</th>
                        <th>Zone</th>
                        <th>Base</th>
                        <th>User</th>
                        <th>Route</th>
                        <th>
                            <a href="img/test.jpg" data-lightbox="outletImage" data-title="Title"> <img
                                        class="outlet_image" src="img/test.jpg"></a>
                            <a href="img/test.jpg" data-lightbox="outletImage" data-title="Title"> <img
                                        class="outlet_image" src="img/test.jpg"></a>

                        </th>
                        <th>
                            <a href="#"><i class="fa fa-pencil btn-info"
                                           style="padding: 5px; border-radius: 5px"></i></a>
                            <a href="#"><i class="fa fa-remove btn-danger" style="padding: 5px; border-radius: 5px"></i></a>
                        </th>
                    </tr>
                    <!--End Data-->


                    <!--Single Data-->
                    <tr class="text-center">
                        <th>01</th>
                        <th>B012H</th>
                        <th>MA Traders</th>
                        <th>South Badda</th>
                        <th>AK Ullash</th>
                        <th>01752046668</th>
                        <th>ak.407@gmail.com</th>
                        <th>NAL</th>
                        <th>Category</th>
                        <th>Biscuit</th>
                        <th>Type Energy</th>
                        <th>1.5 inch</th>
                        <th>2.5 inch</th>
                        <th>4 sft</th>
                        <th>A Grade</th>
                        <th>5.5 Fridge</th>
                        <th>SignBoard</th>
                        <th>Unit</th>
                        <th>Bangladesh</th>
                        <th>Uttara</th>
                        <th>Zone</th>
                        <th>Base</th>
                        <th>User</th>
                        <th>Route</th>
                        <th>
                            <a href="img/test.jpg" data-lightbox="outletImage" data-title="Title"> <img
                                        class="outlet_image" src="img/test.jpg"></a>
                            <a href="img/test.jpg" data-lightbox="outletImage" data-title="Title"> <img
                                        class="outlet_image" src="img/test.jpg"></a>

                        </th>
                        <th>
                            <a href="#"><i class="fa fa-pencil btn-info"
                                           style="padding: 5px; border-radius: 5px"></i></a>
                            <a href="#"><i class="fa fa-remove btn-danger" style="padding: 5px; border-radius: 5px"></i></a>
                        </th>
                    </tr>
                    <!--End Data-->


                    <!--Single Data-->
                    <tr class="text-center">
                        <th>01</th>
                        <th>B012H</th>
                        <th>MA Traders</th>
                        <th>South Badda</th>
                        <th>AK Ullash</th>
                        <th>01752046668</th>
                        <th>ak.407@gmail.com</th>
                        <th>NAL</th>
                        <th>Category</th>
                        <th>Biscuit</th>
                        <th>Type Energy</th>
                        <th>1.5 inch</th>
                        <th>2.5 inch</th>
                        <th>4 sft</th>
                        <th>A Grade</th>
                        <th>5.5 Fridge</th>
                        <th>SignBoard</th>
                        <th>Unit</th>
                        <th>Bangladesh</th>
                        <th>Uttara</th>
                        <th>Zone</th>
                        <th>Base</th>
                        <th>User</th>
                        <th>Route</th>
                        <th>
                            <a href="img/test.jpg" data-lightbox="outletImage" data-title="Title"> <img
                                        class="outlet_image" src="img/test.jpg"></a>
                            <a href="img/test.jpg" data-lightbox="outletImage" data-title="Title"> <img
                                        class="outlet_image" src="img/test.jpg"></a>

                        </th>
                        <th>
                            <a href="#"><i class="fa fa-pencil btn-info"
                                           style="padding: 5px; border-radius: 5px"></i></a>
                            <a href="#"><i class="fa fa-remove btn-danger" style="padding: 5px; border-radius: 5px"></i></a>
                        </th>
                    </tr>
                    <!--End Data-->


                    <!--Single Data-->
                    <tr class="text-center">
                        <th>01</th>
                        <th>B012H</th>
                        <th>MA Traders</th>
                        <th>South Badda</th>
                        <th>AK Ullash</th>
                        <th>01752046668</th>
                        <th>ak.407@gmail.com</th>
                        <th>NAL</th>
                        <th>Category</th>
                        <th>Biscuit</th>
                        <th>Type Energy</th>
                        <th>1.5 inch</th>
                        <th>2.5 inch</th>
                        <th>4 sft</th>
                        <th>A Grade</th>
                        <th>5.5 Fridge</th>
                        <th>SignBoard</th>
                        <th>Unit</th>
                        <th>Bangladesh</th>
                        <th>Uttara</th>
                        <th>Zone</th>
                        <th>Base</th>
                        <th>User</th>
                        <th>Route</th>
                        <th>
                            <a href="img/test.jpg" data-lightbox="outletImage" data-title="Title"> <img
                                        class="outlet_image" src="img/test.jpg"></a>
                            <a href="img/test.jpg" data-lightbox="outletImage" data-title="Title"> <img
                                        class="outlet_image" src="img/test.jpg"></a>

                        </th>
                        <th>
                            <a href="#"><i class="fa fa-pencil btn-info"
                                           style="padding: 5px; border-radius: 5px"></i></a>
                            <a href="#"><i class="fa fa-remove btn-danger" style="padding: 5px; border-radius: 5px"></i></a>
                        </th>
                    </tr>
                    <!--End Data-->


                    <!--Single Data-->
                    <tr class="text-center">
                        <th>01</th>
                        <th>B012H</th>
                        <th>MA Traders</th>
                        <th>South Badda</th>
                        <th>AK Ullash</th>
                        <th>01752046668</th>
                        <th>ak.407@gmail.com</th>
                        <th>NAL</th>
                        <th>Category</th>
                        <th>Biscuit</th>
                        <th>Type Energy</th>
                        <th>1.5 inch</th>
                        <th>2.5 inch</th>
                        <th>4 sft</th>
                        <th>A Grade</th>
                        <th>5.5 Fridge</th>
                        <th>SignBoard</th>
                        <th>Unit</th>
                        <th>Bangladesh</th>
                        <th>Uttara</th>
                        <th>Zone</th>
                        <th>Base</th>
                        <th>User</th>
                        <th>Route</th>
                        <th>
                            <a href="img/test.jpg" data-lightbox="outletImage" data-title="Title"> <img
                                        class="outlet_image" src="img/test.jpg"></a>
                            <a href="img/test.jpg" data-lightbox="outletImage" data-title="Title"> <img
                                        class="outlet_image" src="img/test.jpg"></a>

                        </th>
                        <th>
                            <a href="#"><i class="fa fa-pencil btn-info"
                                           style="padding: 5px; border-radius: 5px"></i></a>
                            <a href="#"><i class="fa fa-remove btn-danger" style="padding: 5px; border-radius: 5px"></i></a>
                        </th>
                    </tr>
                    <!--End Data-->


                    <!--Single Data-->
                    <tr class="text-center">
                        <th>01</th>
                        <th>B012H</th>
                        <th>MA Traders</th>
                        <th>South Badda</th>
                        <th>AK Ullash</th>
                        <th>01752046668</th>
                        <th>ak.407@gmail.com</th>
                        <th>NAL</th>
                        <th>Category</th>
                        <th>Biscuit</th>
                        <th>Type Energy</th>
                        <th>1.5 inch</th>
                        <th>2.5 inch</th>
                        <th>4 sft</th>
                        <th>A Grade</th>
                        <th>5.5 Fridge</th>
                        <th>SignBoard</th>
                        <th>Unit</th>
                        <th>Bangladesh</th>
                        <th>Uttara</th>
                        <th>Zone</th>
                        <th>Base</th>
                        <th>User</th>
                        <th>Route</th>
                        <th>
                            <a href="img/test.jpg" data-lightbox="outletImage" data-title="Title"> <img
                                        class="outlet_image" src="img/test.jpg"></a>
                            <a href="img/test.jpg" data-lightbox="outletImage" data-title="Title"> <img
                                        class="outlet_image" src="img/test.jpg"></a>

                        </th>
                        <th>
                            <a href="#"><i class="fa fa-pencil btn-info"
                                           style="padding: 5px; border-radius: 5px"></i></a>
                            <a href="#"><i class="fa fa-remove btn-danger" style="padding: 5px; border-radius: 5px"></i></a>
                        </th>
                    </tr>
                    <!--End Data-->


                    <!--Single Data-->
                    <tr class="text-center">
                        <th>01</th>
                        <th>B012H</th>
                        <th>MA Traders</th>
                        <th>South Badda</th>
                        <th>AK Ullash</th>
                        <th>01752046668</th>
                        <th>ak.407@gmail.com</th>
                        <th>NAL</th>
                        <th>Category</th>
                        <th>Biscuit</th>
                        <th>Type Energy</th>
                        <th>1.5 inch</th>
                        <th>2.5 inch</th>
                        <th>4 sft</th>
                        <th>A Grade</th>
                        <th>5.5 Fridge</th>
                        <th>SignBoard</th>
                        <th>Unit</th>
                        <th>Bangladesh</th>
                        <th>Uttara</th>
                        <th>Zone</th>
                        <th>Base</th>
                        <th>User</th>
                        <th>Route</th>
                        <th>
                            <a href="img/test.jpg" data-lightbox="outletImage" data-title="Title"> <img
                                        class="outlet_image" src="img/test.jpg"></a>
                            <a href="img/test.jpg" data-lightbox="outletImage" data-title="Title"> <img
                                        class="outlet_image" src="img/test.jpg"></a>

                        </th>
                        <th>
                            <a href="#"><i class="fa fa-pencil btn-info"
                                           style="padding: 5px; border-radius: 5px"></i></a>
                            <a href="#"><i class="fa fa-remove btn-danger" style="padding: 5px; border-radius: 5px"></i></a>
                        </th>
                    </tr>
                    <!--End Data-->


                    <!--Single Data-->
                    <tr class="text-center">
                        <th>01</th>
                        <th>B012H</th>
                        <th>MA Traders</th>
                        <th>South Badda</th>
                        <th>AK Ullash</th>
                        <th>01752046668</th>
                        <th>ak.407@gmail.com</th>
                        <th>NAL</th>
                        <th>Category</th>
                        <th>Biscuit</th>
                        <th>Type Energy</th>
                        <th>1.5 inch</th>
                        <th>2.5 inch</th>
                        <th>4 sft</th>
                        <th>A Grade</th>
                        <th>5.5 Fridge</th>
                        <th>SignBoard</th>
                        <th>Unit</th>
                        <th>Bangladesh</th>
                        <th>Uttara</th>
                        <th>Zone</th>
                        <th>Base</th>
                        <th>User</th>
                        <th>Route</th>
                        <th>
                            <a href="img/test.jpg" data-lightbox="outletImage" data-title="Title"> <img
                                        class="outlet_image" src="img/test.jpg"></a>
                            <a href="img/test.jpg" data-lightbox="outletImage" data-title="Title"> <img
                                        class="outlet_image" src="img/test.jpg"></a>

                        </th>
                        <th>
                            <a href="#"><i class="fa fa-pencil btn-info"
                                           style="padding: 5px; border-radius: 5px"></i></a>
                            <a href="#"><i class="fa fa-remove btn-danger" style="padding: 5px; border-radius: 5px"></i></a>
                        </th>
                    </tr>
                    <!--End Data-->


                    <!--Single Data-->
                    <tr class="text-center">
                        <th>23</th>
                        <th>B012H</th>
                        <th>MA Traders</th>
                        <th>South Badda</th>
                        <th>AK Ullash</th>
                        <th>01752046668</th>
                        <th>ak.407@gmail.com</th>
                        <th>NAL</th>
                        <th>Category</th>
                        <th>Biscuit</th>
                        <th>Type Energy</th>
                        <th>1.5 inch</th>
                        <th>2.5 inch</th>
                        <th>4 sft</th>
                        <th>A Grade</th>
                        <th>5.5 Fridge</th>
                        <th>SignBoard</th>
                        <th>Unit</th>
                        <th>Bangladesh</th>
                        <th>Uttara</th>
                        <th>Zone</th>
                        <th>Base</th>
                        <th>User</th>
                        <th>Route</th>
                        <th>
                            <a href="img/test.jpg" data-lightbox="outletImage" data-title="Title"> <img
                                        class="outlet_image" src="img/test.jpg"></a>
                            <a href="img/test.jpg" data-lightbox="outletImage" data-title="Title"> <img
                                        class="outlet_image" src="img/test.jpg"></a>

                        </th>
                        <th>
                            <a href="#"><i class="fa fa-pencil btn-info"
                                           style="padding: 5px; border-radius: 5px"></i></a>
                            <a href="#"><i class="fa fa-remove btn-danger" style="padding: 5px; border-radius: 5px"></i></a>
                        </th>
                    </tr>
                    <!--End Data-->


                    </tbody>
                </table>
            </div>
        </div>
        <!--/.col (right) -->
        <!--/.col (left) -->


    </div>
    <!-- /.row -->

    <?php include("includes/footer.php"); ?>
    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>

<script type="text/javascript">

    $(document).ready(function () {
        $('#outletInfo').DataTable({
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'pdf',
                    text: '<i class="fa fa-file-pdf-o file_open_icon pdf_icon"></i>',
                    titleAttr: 'PDF',
                    orientation: 'landscape',
                    // exportOptions: {
                    //     modifier: {
                    //         page: 'current'
                    //     }
                    // },
                    header: true,
                    title: 'OUTLET INFORMATION',
                    customize: function(doc) {
                        doc.defaultStyle.fontSize = 8; //<--set font size to 16 instead of 10
                        doc.styles.tableHeader.fontSize = 8
                    }
                },
                {
                    extend: 'excel',
                    text: '<i class="fa fa-file-excel-o file_open_icon excel_icon"></i>',
                    titleAttr: 'Excel',
                    header: true,
                    title: 'OUTLET INFORMATION',
                },
                {
                    extend: 'csv',
                    text: '<i class="fa fa-file-o file_open_icon csv_icon"></i>',
                    titleAttr: 'CSV',
                    header: true,
                    title: 'OUTLET INFORMATION',
                },
                {
                    extend: 'print',
                    text: '<i class="fa fa-print file_open_icon print_icon"></i>',
                    titleAttr: 'Print',
                    orientation: 'landscape',
                    header: true,
                    title: 'OUTLET INFORMATION',
                }
            ]

        });


    });




</script>

<!--Light box js-->
<script type="text/javascript" src="../resource/dist/js/lightbox.js"></script>
</body>
</html>











