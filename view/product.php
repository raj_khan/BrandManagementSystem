<!DOCTYPE html>
<html>
    <?php include("includes/head.php"); ?>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <?php include("includes/header.php"); ?>
            <?php include("includes/sidebar.php"); ?>
            <?php include("includes/breadcrumb.php"); ?>

            <!-- Main content -->
            <!-- Main content -->
            <div class="row">
                <!-- left column -->
                <div class="col-md-6">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Add New Product</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form role="form">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Product Category</label>
                                    <select class="form-control">
                                        <option value="">-- Select Category --</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Supplier Name</label>
                                    <select class="form-control">
                                        <option value="">-- Select Supplier --</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Product Name</label>
                                    <input type="text" class="form-control" id="exampleInputEmail1" >
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Product Code</label>
                                    <input type="text" class="form-control" id="exampleInputEmail1" >
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="exampleInputPassword1">Selling Price</label>
                                    <input type="text" class="form-control" id="exampleInputPassword1" placeholder="">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="exampleInputPassword1">Purchase Price</label>
                                    <input type="text" class="form-control" id="exampleInputPassword1" placeholder="">
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.box -->
                </div>
                <!--/.col (left) -->
                <!-- right column -->
                <div class="col-md-6">
                    <!-- Horizontal Form -->
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Product List</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <table class="table table-responsive">
                            <thead>
                                <tr>
                                    <td>SL</td>
                                    <td>Name</td>
                                    <td>Code</td>
                                    <td>Sell Price</td>
                                    <td>P Price</td>
                                    <td>Action</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th>1</th>
                                    <th>Glass</th>
                                    <th>G-01</th>
                                    <th>100 tk</th>
                                    <th>105 tk</th>
                                    <th>
                                        <a href="#"><i class="fa fa-pencil"></i></a>
                                        <a href="#"><i class="fa fa-check-square"></i></a>
                                        <a href="#"><i class="fa fa-remove"></i></a>
                                    </th>
                                </tr>
                                <tr>
                                    <th>2</th>
                                    <th>Plate</th>
                                    <th>P-01</th>
                                    <th>50</th>
                                    <th>55</th>
                                    <th>
                                        <a href="#"><i class="fa fa-pencil"></i></a>
                                        <a href="#"><i class="fa fa-check-square"></i></a>
                                        <a href="#"><i class="fa fa-remove"></i></a>
                                    </th>
                                </tr>
                                <tr>
                                    <th>3</th>
                                    <th>abc</th>
                                    <th>abc-01</th>
                                    <th>500</th>
                                    <th>550</th>
                                    <th>
                                        <a href="#"><i class="fa fa-pencil"></i></a>
                                        <a href="#"><i class="fa fa-check-square"></i></a>
                                        <a href="#"><i class="fa fa-remove"></i></a>
                                    </th>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!--/.col (right) -->
            </div>
            <!-- /.row -->

            <?php include("includes/footer.php"); ?>
            <!-- Add the sidebar's background. This div must be placed
                 immediately after the control sidebar -->
            <div class="control-sidebar-bg"></div>
        </div>
        <!-- ./wrapper -->
    </body>
</html>











