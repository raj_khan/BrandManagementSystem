<!DOCTYPE html>
<html>
    <?php include("includes/head.php"); ?>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <?php include("includes/header.php"); ?>
            <?php include("includes/sidebar.php"); ?>
            <?php include("includes/breadcrumb.php"); ?>

            <!-- Main content -->
            <!-- Main content -->
            <div class="row">
                <!-- left column -->
                <div class="col-md-8">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">PO Info</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form role="form">
                            <div class="box-body">
                                <div class="form-group col-md-3">
                                    <label for="exampleInputEmail1">Supplier</label>
                                    <select class="form-control">
                                        <option value="">-- Select Supplier --</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="exampleInputEmail1">Supplier Code</label>
                                    <input type="text" class="form-control" id="exampleInputEmail1" >
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="exampleInputPassword1">PO No</label>
                                    <input type="text" class="form-control" id="exampleInputPassword1" placeholder="">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="exampleInputPassword1">PO Date</label>
                                    <input type="text" class="form-control" id="exampleInputPassword1" placeholder="">
                                </div>
                            </div>
                            <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Product Add</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <table class="table table-responsive">
                            <thead>
                                <tr>
                                    <td>SL</td>
                                    <td>Product</td>
                                    <td>Quantity</td>
                                    <td>Unit Price</td>
                                    <td>Total Price</td>
                                    <td><button type="button" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i></button></td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th>1</th>
                                    <th>
                                        <select class="form-control">
                                            <option>Select Product</option>
                                        </select>
                                    </th>
                                    <th>
                                        <input type="text" class="form-control" name="quantity">
                                    </th>
                                    <th>
                                        <input type="text" class="form-control" name="quantity">
                                    </th>
                                    <th>
                                        <input type="text" class="form-control" name="quantity">
                                    </th>
                                    <th>
                                    </th>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.box -->
                </div>
            </div>
            <!-- /.row -->

            <?php include("includes/footer.php"); ?>
            <!-- Add the sidebar's background. This div must be placed
                 immediately after the control sidebar -->
            <div class="control-sidebar-bg"></div>
        </div>
        <!-- ./wrapper -->
    </body>
</html>











